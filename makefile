CC=g++

SRC := sources
OBJ := objects

SOURCES := $(wildcard $(SRC)/*.cpp)
OBJECTS := $(patsubst $(SRC)/%.cpp, $(OBJ)/%.o, $(SOURCES))

recommendation: $(OBJECTS)
	$(CC) -g -Wall algorithm/recommendation.cpp $^ -o $@

$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) -g -I$(SRC) -c $< -o $@

clean:
	@rm -f D ./objects/*.o core
	@rm -f recommendation
	@rm -f
