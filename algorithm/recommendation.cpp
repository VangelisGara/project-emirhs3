#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <unordered_map>
#include <vector>
#include "../headers/RecommendationSystem.h"
using namespace std;

int main(int argc, char *argv[]){
  // Arguments
  int p=20; // nearest neighbors
  string input_file,output_file,vect_space,recomm_method;
  string a_lexicon = "./lexicons/vader_lexicon.csv",k_lexicon = "./lexicons/coins_queries.csv";
  string clusters_file = "./datasets/td-idf500-subset";
  vector<string> Arguments(argv, argv + argc); // store argument here
  int validate=0;

	// Get args.
	for( int i = 0; i < argc; i++){
    if( Arguments[i] == "-m")
      recomm_method = Arguments[i+1];
    if( Arguments[i] == "-v")
      vect_space = Arguments[i+1];
		if( Arguments[i] == "-d")
  			input_file = Arguments[i+1];
		if( Arguments[i] == "-o")
  			output_file = Arguments[i+1];
		if( Arguments[i] == "-p")
  			p = stoi(Arguments[i+1]);
	  if( Arguments[i] == "-validate")
      	validate = 1;
	}
  cout << input_file << "," << output_file << "," << p << "," << validate << endl;
  if(!validate){ // recommend cryptos to users
    if( recomm_method == "range-search"){
      // Recommendation System is being set up
      RecommendationSystem rs(input_file,a_lexicon,k_lexicon,clusters_file,vect_space);
      rs.start_recommendationLSH(vect_space,output_file,p);
    }
    else if( recomm_method == "clustering"){
      // Recommendation System is being set up
        RecommendationSystem rs(input_file,a_lexicon,k_lexicon,clusters_file,vect_space);
        rs.start_recommendationClustering(vect_space,output_file);
      }
    else
      cout << "Please specify recommendation method and vector space to use" << endl;
  }
  else{ // validation score for the recommendation algorithms
    if( recomm_method == "range-search"){
      // Recommendation System is being set up
      RecommendationSystem rs(input_file,a_lexicon,k_lexicon,clusters_file,vect_space);
      rs.validate_recommendationLSH(vect_space,output_file,p);
    }
    else if( recomm_method == "clustering"){
      RecommendationSystem rs(input_file,a_lexicon,k_lexicon,clusters_file,vect_space);
      rs.validate_recommendationClustering(vect_space,output_file);
    }
    else
      cout << "Please specify recommendation method and vector space to use" << endl;
  }
}
