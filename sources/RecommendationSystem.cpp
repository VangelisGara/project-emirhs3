#include <algorithm>
#include <chrono>
#include <cmath>
#include <cstdio>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <random>
#include <unordered_map>
#include <vector>
#include "../headers/Clustering.h"
#include "../headers/HashTable.h"
#include "../headers/Insights.h"
#include "../headers/MathHelper.h"
#include "../headers/RecommendationSystem.h"
#include "../headers/VectorSpace.h"
using namespace std;

int memory_usage=0;

// Search in vector and return its index
template < typename T>
pair<bool,int> findInVector(const vector<T>  & vecOfElements, const T & element){
	pair<bool,int> result;
	// Find given element in vector
	auto it = std::find(vecOfElements.begin(), vecOfElements.end(), element);
	if (it != vecOfElements.end()){
		result.second = distance(vecOfElements.begin(), it);
		result.first = true;
	}
	else{
		result.first = false;
		result.second = -1;
	}
	return result;
}

// create our required data structures
RecommendationSystem::RecommendationSystem(string tweets_path,string lexicon_path,string cryptos_path,string clusters_file,string vector_space){
  lexicon_analyzer(lexicon_path); // initialize our vader lexicon
  cryptocurrencies_analyzer(cryptos_path); // initialize our cryptocurrency lexicon
	tweets_analysis(tweets_path); // get the tweets back processed
	if(vector_space == "clusters")
		clusters_analysis(clusters_file); // get clusters' profile
	// debug
	//print_lexicons();
  //print_users();
	//print_clusters();
}

// gets a csv lexicon as an argument and returns a dictionary like umap
void RecommendationSystem::lexicon_analyzer(string lexicon_path){
  ifstream csv_file;
  string line,word_score;
  // open file
  csv_file.open(lexicon_path);
  if(csv_file.fail()){
    cout << "Error opening file: " << lexicon_path << endl;
    exit(-1);
  }
  // for every line
  while(!csv_file.eof()){
    getline(csv_file,line);
    istringstream iss(line); // analyze the string to get the word and the score
    int i=0;
    double score;
    string word;
    while(getline(iss,word_score,'\t')){
      if( i == 0)
        word = word_score;
      else
        score = stod(word_score);
      i++;
    }
    vader_lexicon[word] = score; // add word to lexicon
  }
  csv_file.close();
}

// gets a cryptocurrency lexicon and returns it to vector of vectors
void RecommendationSystem::cryptocurrencies_analyzer(string cryptos_path){
  ifstream csv_file;
  string line,cryptocurrency_name;
  // open file
  csv_file.open(cryptos_path);
  if(csv_file.fail()){
    cout << "Error opening file: " << cryptos_path << endl;
    exit(-1);
  }
  // for every line
  while(!csv_file.eof()){
    getline(csv_file,line);
    if(line.empty())
      continue;
    istringstream iss(line); // analyze the string to get the different cryptos names
    cryptocurrencies.push_back(vector <string>()); // push back an empty vector
    int nu = 0 ;
    while(iss >> cryptocurrency_name)
      cryptocurrencies[numCryptos].push_back(cryptocurrency_name);
    numCryptos++;
  }
  csv_file.close();
}

// compute sentiment score for each tweet and the crypto it refers to and for each user create a profile
void RecommendationSystem::tweets_analysis(string tweets_path){
  // Process dataset
  ifstream csv_file;
  string tweet,user,tweet_id,word;
  int alpha = 15;
  // open file
  csv_file.open(tweets_path);
  if(csv_file.fail()){
    cout << "Error opening file: " << tweets_path << endl;
    exit(-1);
  }
  // for every line
  while(!csv_file.eof()){
    //cout << "---------------------------------------------------------------" << endl;
    // analyze the string to get the different cryptos names
    getline(csv_file,tweet);
    istringstream iss(tweet);
    iss >> user; // get user id
    //cout << user << ",";
    iss >> tweet_id; // get tweet id
    //cout << tweet_id << ",";
    unordered_map<string,double>::const_iterator found; // sentiment analysis begins here
    double totalscore=0; // tweet's sentiment score
    vector<int> crypto; // cryptos it refers to
    while(iss >> word){
      found = vader_lexicon.find(word);
      //cout << word << endl;
      if ( found != vader_lexicon.end() ){
        //cout << "Found in vader lexicon with score" << " : " << found->second << endl;
        totalscore += found->second;
      }
      else{
        for(size_t n=0; n<cryptocurrencies.size(); ++n){
          auto i = find(cryptocurrencies[n].begin(), cryptocurrencies[n].end(), word);
          if(cryptocurrencies[n].end() != i){
            //cout << "Found in cryptos at row " << n << " col " << i-cryptos[n].begin() << endl;
            crypto.push_back(n);
          }
        }
      }
    }
    double Si = totalscore/sqrt((totalscore*totalscore)+alpha);
    /*cout << Si << " -> ";
    for (int c = 0; c < crypto.size(); c++)
      cout << crypto[c] << ' ';
    getchar();*/
    proccessed_tweets.insert(make_pair(stoi(tweet_id),make_pair(Si,crypto)));
    // create users profile
    pair<bool,int> result = findInVector<int>(usersIDs,stoi(user));
    if(result.first){ // if user is found
      //cout << " Found at " << result.second << endl;
      for(auto crypt: crypto){
				users_tweets[result.second].push_back(crypt);
			  users[result.second][crypt] += Si;
			}
		}
    else{
      //cout << "Not found " << endl;
      usersIDs.push_back(stoi(user));
			vector<int> user_tweets;
      vector<double> user_opinion(numCryptos,0.0);
      for(auto crypt: crypto){
				user_tweets.push_back(crypt);
			  user_opinion[crypt] += Si;
			}
      users.push_back(user_opinion);
			users_tweets.push_back(user_tweets);
    }
  }
  csv_file.close();
}

// create a profile for each cluster
void RecommendationSystem::clusters_analysis(string clusters_path){
	ifstream results_file;
	string line;
	unordered_map<int,pair<double,vector<int>>>::const_iterator found;
	// open file
	results_file.open(clusters_path);
	if(results_file.fail()){
		cout << "Error opening file: " << clusters_path << endl;
		exit(-1);
	} // error handling
	for(int i=1; i<=4; i++) // skip the first three lines
		getline(results_file,line);
	bool flag=false; // variable that helps results processing
	while(!results_file.eof()){
		getline(results_file,line);
		// skip clustering's metadata
		size_t found_meta = line.find("clustering_time");
		if(found_meta != string::npos)
			break;
		flag = !flag;
		//cout << line << endl;
		if(flag){ // get cluster's id
			int cluster_id;
			sscanf(line.c_str(),"CLUSTER-%d",&cluster_id);
			//cout << cluster_id;
			clustersIDs.push_back(cluster_id); // push back cluster's id
		}
		else{ // create a profile for each cluster
			vector<double> cluster_opinion(numCryptos,0.0); // clusters profile about cryptocurrencies
			// get the content between brackets
			unsigned open = line.find("{") + 1;
			unsigned close = line.find(",}") - 1;
			string pointsIds = line.substr(open,close);
			//cout << pointsIds << endl;
			// get the id of each point in cluster
			stringstream ss(pointsIds);
			string pointId;
			vector<int> cluster_cryptos;
			while(getline(ss,pointId,',')){
				//cout << pointId << " ";
				found = proccessed_tweets.find(stoi(pointId)); // find tweet's info
				if ( found != proccessed_tweets.end() ){
					double sentiment_score = found->second.first;
					vector<int> cryptos_referring = found->second.second;
					//cout << endl <<"Found in processed tweets with score" << " : " << sentiment_score << " and refering to crypto: ";
					for(int i=0; i<cryptos_referring.size(); i++){
						//cout << cryptos_referring[i] << ",";
						cluster_opinion[cryptos_referring[i]] += sentiment_score;
						cluster_cryptos.push_back(cryptos_referring[i]);
					}
				}
				//cout << endl;
			}
			clusters_tweets.push_back(cluster_cryptos);
			clusters.push_back(cluster_opinion);
		}
	}
	results_file.close();
}

// start recommending cryptos to users with LSH cosine Range Search
void RecommendationSystem::start_recommendationLSH(string vectors,string output_file,int p){
	// recommendation based on user opinions after sentiment analysis
	ofstream out;
	out.open(output_file);
	auto start = chrono::steady_clock::now();
	if(vectors == "users"){
		out << "Cosine LSH on users:" << endl;
		// Unrated users will get the mean score of the rated cryptos
		for(int i=0; i<users.size(); i++){
			double R=0; // average score
			int rated_items=0; // number of rated items
			for(int j=0; j<users[i].size(); j++){
				// find those tweet that user has a rating
				if(find(users_tweets[i].begin(),users_tweets[i].end(),j) != users_tweets[i].end()){
					rated_items++;
					R += users[i][j];
				}
			}
			if(rated_items !=0)
				R /= rated_items;
			for(int j=0; j<users[i].size(); j++){
				if(find(users_tweets[i].begin(),users_tweets[i].end(),j) == users_tweets[i].end()){
					users[i][j] =  R;
				}
			}
		}

		// Create the Vector Space
		VectorSpace users_vector_space(&usersIDs,&users);

		// Create the Hash Tables And Hash Vector Space
		int k=4, L=5;
		string metric = "cosine";
		vector<int> r;
  	for(int i=0; i<k ; i++)
    	r.push_back((rand() % 10));
		vector <HashTable> LTables ;
		for(int i=0; i<L; i++){
	    HashTable Li(k,r,users_vector_space.getDimension()); // create a hash table
	    LTables.push_back(Li); // push it to the vector with the hash tables
	    LTables[i].HashVectorSpace(&users_vector_space,k,metric); // fill each hash table after hashing a vector space
	  }
		vector<int> Result,results;

		// Recommend each user 5 cryptos
		for(int Ui=0; Ui<users.size(); Ui++){
			// for users that doesn't have an opinion for any crypto there is no way recommending cryptos
			if((all_of(users[Ui].begin(), users[Ui].end(), [](double i) { return i==0.0; })))
				continue;

			// LSH-Cosine Range Search
		  for(int l=0 ; l<L ; l++)
			{
				// get user's nearest neighbors
				results = (LTables)[l].RangeSearch(0,&users_vector_space,&(users[Ui]),k,users_vector_space.getNumberOfPoints(),metric);
				int points_in_bucket = 0;
				for(uint j=0 ; j < results.size() ; j++){
					Result.push_back(results[j]); // get all neighbors
				}
			}
			sort(Result.begin(),Result.end());
			Result.erase(unique(Result.begin(),Result.end()),Result.end());
			if(Result.size() == 0)
				cout << "Empty" << endl;

			// Keep the P closest neighbors-users
			vector<vector<double>> neighboring_users;
			vector<double> nearest_neighbors_dists;
			vector<int> nearest_neighbors_ids;
			for(uint p1 = 0 ; p1 < Result.size() ; p1++){
				if(neighboring_users.size() < p){
					// check if users has only zeros
					if(all_of(users[Result[p1]].begin(), users[Result[p1]].end(), [](double i) { return i==0.0; }))
						continue;
					neighboring_users.push_back(users[Result[p1]]);
					nearest_neighbors_dists.push_back(cosine_distance(&users[Result[p1]],&users[Ui]));
					nearest_neighbors_ids.push_back(Result[p1]);
				}
				else{
					// check if users has only zeros
					if(all_of(users[Result[p1]].begin(), users[Result[p1]].end(), [](double i) { return i==0.0; }))
						continue;
					int max_index;
					double max_distance = -1;
					// find the neighbor that is the furthest
					for(int p2=0; p2<p; p2++){
						if( nearest_neighbors_dists[p2] > max_distance){
							max_distance = nearest_neighbors_dists[p2];
							max_index = p2;
						}
					}
					// check if current neighbor is closest than furthest
					double curr_distance = cosine_distance(&users[Result[p1]],&users[Ui]);
					if(curr_distance < max_distance){
						neighboring_users[max_index] = users[Result[p1]];
						nearest_neighbors_dists[max_index] = curr_distance;
						nearest_neighbors_ids[max_index] = Result[p1];
					}
				}
			}

			// Νormalization: user's vector
			vector<double> user = users[Ui];
			double R=0; // average score
			int rated_items=0; // number of rated items
			for(int j=0; j<user.size(); j++){
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),j) != users_tweets[Ui].end()){
					rated_items++;
					R += user[j];
				}
			}
			if(rated_items !=0)
				R /= rated_items;
			for(int j=0; j<user.size(); j++){
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),j) != users_tweets[Ui].end()){
					user[j] = user[j] - R;
				}
				else{
					user[j] = 0;
				}
			}

			// Νormalization: neighbors' vector
			for(int i=0; i<neighboring_users.size(); i++){
				double R=0; // average score
				int rated_items=0; // number of rated items
				int nn_id = nearest_neighbors_ids[i]; // the id of neighbor
				for(int j=0; j<neighboring_users[i].size(); j++){
					if(find(users_tweets[nn_id].begin(),users_tweets[nn_id].end(),j) != users_tweets[nn_id].end()){
						rated_items++;
						R += neighboring_users[i][j];
					}
				} // calculate average
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j<neighboring_users[i].size(); j++){
					if(find(users_tweets[nn_id].begin(),users_tweets[nn_id].end(),j) != users_tweets[nn_id].end()){
						neighboring_users[i][j] = neighboring_users[i][j] - R;
					}
					else{
						neighboring_users[i][j] = 0;
					}
				}
			}

			// Predict Scores with P Nearest Neighbors
			for(int i=0; i<user.size(); i++){
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),i) != users_tweets[Ui].end())
					continue;
				double z=0;
				for(uint p1 = 0 ; p1 < neighboring_users.size() ; p1++){ // for each neighbor
					// For each cryptocurrency that user doesn't have opinion
						double sim = (1-nearest_neighbors_dists[p1]); // calculate similarity based pre-normalization
						//double sim = similarity(&user,&neighboring_users[p1]);
						user[i] += sim*neighboring_users[p1][i];
						z += abs(sim);
				}
				z = 1/z;
				double predict_score = z*user[i];
				user[i] = predict_score;
			}

			// Find the 5 best cryptocurrencies
			vector<int> best_cryptos;
			vector<double> best_cryptos_scores;
			for(int c=0; c<user.size(); c++){
				// we need to reccomend bitcoins that user has never refered to
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),c) != users_tweets[Ui].end())
					continue;
				if(best_cryptos.size() < 5){
					best_cryptos.push_back(c);
					best_cryptos_scores.push_back(user[c]);
				}
				else{
					int min_index;
					double min_score = 100000;
					// find the worst score
					for(int j=0; j<5; j++){
						if( best_cryptos_scores[j] < min_score ){
							min_score = best_cryptos_scores[j];
							min_index = j;
						}
					}
					// check if current score is better than worse
					if( user[c] >  min_score){
						best_cryptos_scores[min_index] = user[c];
						best_cryptos[min_index] = c;
					}
				}
			}
			cout << endl << "----------" << endl;
			cout << "User: " << usersIDs[Ui] << endl;
			out << usersIDs[Ui] << ": ";
			cout << "Best Scores: " << endl;
			for(int c=0; c<best_cryptos_scores.size(); c++){
				cout << cryptocurrencies[best_cryptos[c]][0] << endl;
				out << cryptocurrencies[best_cryptos[c]][0] << " ";
				//cout << best_cryptos_scores[c] << endl;
			}
			out << endl;
			cout << endl;
		}
	}
	// recommendation based on TF-IDF text processing on tweets
	if(vectors == "clusters"){
		out << "Cosine LSH on clusters:" << endl;
		auto start = std::chrono::system_clock::now();
		// Unrated users will get the mean score
		for(int i=0; i<clusters.size(); i++){
			double R=0; // average score
			int rated_items=0; // number of rated items
			for(int j=0; j<clusters[i].size(); j++){
				if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) != clusters_tweets[i].end()){
					rated_items++;
					R += clusters[i][j];
				}
			}
			if(rated_items !=0)
				R /= rated_items;
			for(int j=0; j<users[i].size(); j++){
				if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) == clusters_tweets[i].end()){
					clusters[i][j] =  R;
				}
			}
		}

		// Create the Vector Space
		VectorSpace users_vector_space(&clustersIDs,&clusters);

		// Create the Hash Tables And Hash Vector Space
		int k=4, L=5;
		string metric = "cosine";
		vector<int> r;
		for(int i=0; i<k ; i++)
			r.push_back((rand() % 10));
		vector <HashTable> LTables ;
		for(int i=0; i<L; i++){
			HashTable Li(k,r,users_vector_space.getDimension()); // create a hash table
			LTables.push_back(Li); // push it to the vector with the hash tables
			LTables[i].HashVectorSpace(&users_vector_space,k,metric); // fill each hash table after hashing a vector space
		}
		vector<int> Result,results;

		// Recommend each user 5 cryptos
		for(int Ui=0; Ui<users.size(); Ui++){
			// for users that doesn't have opinion there is no way recommending cryptos
			if((all_of(users[Ui].begin(), users[Ui].end(), [](double i) { return i==0.0; })))
				continue;

			// LSH-Cosine Range Search
			for(int l=0 ; l<L ; l++)
			{
				results = (LTables)[l].RangeSearch(0,&users_vector_space,&(users[Ui]),k,users_vector_space.getNumberOfPoints(),metric); // get nearest neighbors
				int points_in_bucket = 0;
				for(uint j=0 ; j < results.size() ; j++){
					Result.push_back(results[j]); // get all neighbors
				}
			}
			sort(Result.begin(),Result.end());
			Result.erase(unique(Result.begin(),Result.end()),Result.end());
			if(Result.size() == 0)
				cout << "Empty" << endl;

			// Keep the P closest neighbors-users
			vector<vector<double>> neighboring_users;
			vector<double> nearest_neighbors_dists;
			vector<int> nearest_neighbors_ids;
			for(uint p1 = 0 ; p1 < Result.size() ; p1++){
				if(neighboring_users.size() < p){
					// check if users has only zeros
					if(all_of(clusters[Result[p1]].begin(), clusters[Result[p1]].end(), [](double i) { return i==0.0; }))
						continue;
					neighboring_users.push_back(clusters[Result[p1]]);
					nearest_neighbors_dists.push_back(cosine_distance(&clusters[Result[p1]],&users[Ui]));
					nearest_neighbors_ids.push_back(Result[p1]);
				}
				else{
					// check if users has only zeros
					if(all_of(clusters[Result[p1]].begin(), clusters[Result[p1]].end(), [](double i) { return i==0.0; }))
						continue;
					int max_index;
					double max_distance = -1;
					// find the neighbor that is the furthest
					for(int p2=0; p2<p; p2++){
						if( nearest_neighbors_dists[p2] > max_distance){
							max_distance = nearest_neighbors_dists[p2];
							max_index = p2;
						}
					}
					// check if current neighbor is closest than furthest
					double curr_distance = cosine_distance(&clusters[Result[p1]],&users[Ui]);
					if(curr_distance < max_distance){
						neighboring_users[max_index] = clusters[Result[p1]];
						nearest_neighbors_dists[max_index] = curr_distance;
						nearest_neighbors_ids[max_index] = Result[p1];
					}
				}
			}

			// Νormalization: user's vector
			vector<double> user = users[Ui];
			double R=0; // average score
			int rated_items=0; // number of rated items
			for(int j=0; j<user.size(); j++){
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),j) != users_tweets[Ui].end()){
					rated_items++;
					R += user[j];
				}
			}
			if(rated_items !=0)
				R /= rated_items;
			for(int j=0; j<user.size(); j++){
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),j) != users_tweets[Ui].end()){
					user[j] = user[j] - R;
				}
				else{
					user[j] = 0;
				}
			}

			// Νormalization: neighbors' vector
			for(int i=0; i<neighboring_users.size(); i++){
				double R=0; // average score
				int rated_items=0; // number of rated items
				int nn_id = nearest_neighbors_ids[i]; // the id of neighbor
				for(int j=0; j<neighboring_users[i].size(); j++){
					if(find(clusters_tweets[nn_id].begin(),clusters_tweets[nn_id].end(),j) != clusters_tweets[nn_id].end()){
						rated_items++;
						R += neighboring_users[i][j];
					}
				} // calculate average
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j<neighboring_users[i].size(); j++){
					if(find(clusters_tweets[nn_id].begin(),clusters_tweets[nn_id].end(),j) != clusters_tweets[nn_id].end()){
						neighboring_users[i][j] = neighboring_users[i][j] - R;
					}
					else{
						neighboring_users[i][j] = 0;
					}
				}
			}

			// Predict Scores with P Nearest Neighbors
			for(int i=0; i<user.size(); i++){
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),i) != users_tweets[Ui].end())
					continue;
				double z=0;
				for(uint p1 = 0 ; p1 < neighboring_users.size() ; p1++){ // for each neighbor
					// For each cryptocurrency that user doesn't have opinion
						double sim = (1-nearest_neighbors_dists[p1]); // calculate similarity based pre-normalization
						//double sim = similarity(&user,&neighboring_users[p1]);
						user[i] += sim*neighboring_users[p1][i];
						z += abs(sim);
				}
				z = 1/z;
				double predict_score = z*user[i];
				user[i] = predict_score;
			}

			// Find the 2 best cryptocurrencies
			vector<int> best_cryptos;
			vector<double> best_cryptos_scores;
			for(int c=0; c<user.size(); c++){
				// we need to reccomend bitcoins that user has never refered to
				if(find(users_tweets[Ui].begin(),users_tweets[Ui].end(),c) != users_tweets[Ui].end())
					continue;
				if(best_cryptos.size() < 2){
					best_cryptos.push_back(c);
					best_cryptos_scores.push_back(user[c]);
				}
				else{
					int min_index;
					double min_score = 100000;
					// find the worst score
					for(int j=0; j<2; j++){
						if( best_cryptos_scores[j] < min_score ){
							min_score = best_cryptos_scores[j];
							min_index = j;
						}
					}
					// check if current score is better than worse
					if( user[c] >  min_score){
						best_cryptos_scores[min_index] = user[c];
						best_cryptos[min_index] = c;
					}
				}
			}
			cout << "--------------------" <<endl;
			cout << "User: " << usersIDs[Ui] << endl;
			out << usersIDs[Ui] << ": ";
			cout << "Best Scores: " << endl;
			for(int c=0; c<best_cryptos_scores.size(); c++){
				cout << cryptocurrencies[best_cryptos[c]][0] << " ";
				out << cryptocurrencies[best_cryptos[c]][0] << " ";
				//cout << best_cryptos_scores[c] << endl;
			}
			out << endl;
			cout << endl;
		}

	}
	auto end = chrono::steady_clock::now();
	auto elapsed = end - start;
	cout << "Execution time : " << chrono::duration_cast<chrono::seconds>(end - start).count() << " sec";
	out << "Execution time : " << chrono::duration_cast<chrono::seconds>(end - start).count() << " sec" << endl;
	out.close();
}

// validate the recommendation of lsh cosine range search method
void RecommendationSystem::validate_recommendationLSH(string vector_space,string output_file,int p){
	// 10-fold cross validation on users
	ofstream out;
	out.open(output_file);
	if(vector_space=="users"){
		// store all rated items
		vector<pair <int,int>> pairs;
		for(int Ui=0; Ui<users.size(); Ui++){ // for each user
			for(int i=0; i<users_tweets[Ui].size(); i++){ // for each crypto referring
				pair<int,int> a_pair(Ui, users_tweets[Ui][i]);
				if (find(pairs.begin(), pairs.end(), a_pair) == pairs.end()) {
					pairs.push_back(a_pair); // (user,crypto)
				}
			}
		}
		// split pairs to 10 subsets for 10-fold cross validation
		int number_of_pairs = pairs.size();
		int subset_size = number_of_pairs/10;
		vector<int> pairs_indexes;
		for(int i=0; i<number_of_pairs; i++) // store all indexes
			pairs_indexes.push_back(i);
		unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // shuffle them
		     ::shuffle(pairs_indexes.begin(), pairs_indexes.end(), default_random_engine(seed));
		vector<vector<int>> subsets(10,vector<int>());
		int offset = 0;
		for(int i=0; i<10; i++){ // load each subset
			vector<int> subset(&pairs_indexes[offset],&pairs_indexes[offset+subset_size]);
			subsets[i] = subset;
			offset += offset + 1;
		}

		// 10-fold cross validation
		double average_MAE = 0;
		for(int validation_set=0; validation_set<10; validation_set++){
			// remove actual ratings, so that we can predict them
			vector<vector <double>> edit_users = users;
			vector<vector<int>> edit_users_tweets = users_tweets;

			// edit the user's space, so that we can predict items
			int n = 0;
			for(int i=0; i<subsets[validation_set].size(); i++){
				//cout << "Subset-" << i << endl;
				pair<int,int> pr = pairs[subsets[validation_set][i]];
				edit_users[pr.first][pr.second] = 0;
				n++;
				edit_users_tweets[pr.first].erase(remove(edit_users_tweets[pr.first].begin(), edit_users_tweets[pr.first].end(), pr.second), edit_users_tweets[pr.first].end());
			}

		 						// Predict erased values
			// unrated users will get the mean score of the rated cryptos
			for(int i=0; i<edit_users.size(); i++){
				double R=0; // average score
				int rated_items=0; // number of rated items
				for(int j=0; j<edit_users[i].size(); j++){
					// find those tweet that user has a rating
					if(find(edit_users_tweets[i].begin(),edit_users_tweets[i].end(),j) != edit_users_tweets[i].end()){
						rated_items++;
						R += edit_users[i][j];
					}
				}
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j<edit_users[i].size(); j++){
					if(find(edit_users_tweets[i].begin(),edit_users_tweets[i].end(),j) == edit_users_tweets[i].end()){
						edit_users[i][j] =  R;
					}
				}
			}

			// create the Vector Space
			VectorSpace users_vector_space(&usersIDs,&edit_users);

			// create the Hash Tables And Hash Vector Space
			int k=4, L=5;
			string metric = "cosine";
			vector<int> r;
			for(int i=0; i<k ; i++)
				r.push_back((rand() % 10));
			vector <HashTable> LTables ;
			for(int i=0; i<L; i++){
				HashTable Li(k,r,users_vector_space.getDimension()); // create a hash table
				LTables.push_back(Li); // push it to the vector with the hash tables
				LTables[i].HashVectorSpace(&users_vector_space,k,metric); // fill each hash table after hashing a vector space
			}
			vector<int> Result,results;

			// for each user in validation subset predict the erased value
			double MAE = 0; // mean absolute error
			for(int i=0; i<subsets[validation_set].size(); i++){
				pair<int,int> pr = pairs[subsets[validation_set][i]];
				if((all_of(edit_users[pr.first].begin(), edit_users[pr.first].end(), [](double i) { return i==0.0; })))
					continue;
				// LSH-Cosine Range Search
				for(int l=0 ; l<L ; l++){
					// get user's nearest neighbors
					results = (LTables)[l].RangeSearch(0,&users_vector_space,&(edit_users[pr.first]),k,users_vector_space.getNumberOfPoints(),metric);
					int points_in_bucket = 0;
					for(uint j=0; j < results.size(); j++){
						Result.push_back(results[j]); // get all neighbors
					}
				}
				sort(Result.begin(),Result.end());
				Result.erase(unique(Result.begin(),Result.end()),Result.end());
				if(Result.size() == 0)
					cout << "Empty" << endl;

				// Keep the P closest neighbors-users
				vector<vector<double>> neighboring_users;
				vector<double> nearest_neighbors_dists;
				vector<int> nearest_neighbors_ids;
				for(uint p1 = 0 ; p1 < Result.size() ; p1++){
					if(neighboring_users.size() < p){
						// check if users has only zeros
						if(all_of(edit_users[Result[p1]].begin(), edit_users[Result[p1]].end(), [](double i) { return i==0.0; }))
							continue;
						neighboring_users.push_back(edit_users[Result[p1]]);
						nearest_neighbors_dists.push_back(cosine_distance(&edit_users[Result[p1]],&edit_users[pr.first]));
						nearest_neighbors_ids.push_back(Result[p1]);
					}
					else{
						// check if users has only zeros
						if(all_of(edit_users[Result[p1]].begin(), edit_users[Result[p1]].end(), [](double i) { return i==0.0; }))
							continue;
						int max_index;
						double max_distance = -1;
						// find the neighbor that is the furthest
						for(int p2=0; p2<p; p2++){
							if( nearest_neighbors_dists[p2] > max_distance){
								max_distance = nearest_neighbors_dists[p2];
								max_index = p2;
							}
						}
						// check if current neighbor is closest than furthest
						double curr_distance = cosine_distance(&edit_users[Result[p1]],&edit_users[pr.first]);
						if(curr_distance < max_distance){
							neighboring_users[max_index] = edit_users[Result[p1]];
							nearest_neighbors_dists[max_index] = curr_distance;
							nearest_neighbors_ids[max_index] = Result[p1];
						}
					}
				}

				// Νormalization: user's vector
				vector<double> user = edit_users[pr.first];
				double R=0; // average score
				int rated_items=0; // number of rated items
				for(int j=0; j<user.size(); j++){
					if(find(edit_users_tweets[pr.first].begin(),edit_users_tweets[pr.first].end(),j) != edit_users_tweets[pr.first].end()){
						rated_items++;
						R += user[j];
					}
				}
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j<user.size(); j++){
					if(find(edit_users_tweets[pr.first].begin(),edit_users_tweets[pr.first].end(),j) != edit_users_tweets[pr.first].end()){
						user[j] = user[j] - R;
					}
					else{
						user[j] = 0;
					}
				}

				// Νormalization: neighbors' vector
				for(int i=0; i<neighboring_users.size(); i++){
					double R=0; // average score
					int rated_items=0; // number of rated items
					int nn_id = nearest_neighbors_ids[i]; // the id of neighbor
					for(int j=0; j<neighboring_users[i].size(); j++){
						if(find(edit_users_tweets[nn_id].begin(),edit_users_tweets[nn_id].end(),j) != edit_users_tweets[nn_id].end()){
							rated_items++;
							R += neighboring_users[i][j];
						}
					} // calculate average
					if(rated_items !=0)
						R /= rated_items;
					for(int j=0; j<neighboring_users[i].size(); j++){
						if(find(edit_users_tweets[nn_id].begin(),edit_users_tweets[nn_id].end(),j) != edit_users_tweets[nn_id].end()){
							neighboring_users[i][j] = neighboring_users[i][j] - R;
						}
						else{
							neighboring_users[i][j] = 0;
						}
					}
				}

				// Predict Scores with P Nearest Neighbors
				double z=0;
				for(uint p1 = 0 ; p1 < neighboring_users.size() ; p1++){ // for each neighbor
					// For each cryptocurrency that user doesn't have opinion
						double sim = (1-nearest_neighbors_dists[p1]); // calculate similarity based pre-normalization
						//double sim = similarity(&user,&neighboring_users[p1]);
						user[pr.second] += sim*neighboring_users[p1][pr.second];
						z += abs(sim);
				}
				z = 1/z;
				double predict_score = z*user[pr.second];

				MAE += abs(users[pr.first][pr.second] - predict_score);
				//getchar();
			}
			MAE /= subsets[validation_set].size();
			average_MAE += MAE;
		}
		average_MAE /= 10;
		cout << "Cosine LSH recommendation, based on real users, MAE: " << average_MAE << endl;
		out << "Cosine LSH recommendation, based on real users, MAE: " << average_MAE << endl;
	}
	// random validation on clusters
	if(vector_space=="clusters"){
		// store all rated items
		vector<pair <int,int>> pairs;
		for(int Ui=0; Ui<users.size(); Ui++){ // for each user
			for(int i=0; i<users_tweets[Ui].size(); i++){ // for each crypto referring
				pair<int,int> a_pair(Ui, users_tweets[Ui][i]);
				if (find(pairs.begin(), pairs.end(), a_pair) == pairs.end()) {
					pairs.push_back(a_pair); // (user,crypto)
				}
			}
		}
		// split pairs to 10 subsets for 10-fold cross validation
		int number_of_pairs = pairs.size();
		int subset_size = number_of_pairs/10;
		vector<int> pairs_indexes;
		for(int i=0; i<number_of_pairs; i++) // store all indexes
			pairs_indexes.push_back(i);

		// validation
		double average_MAE = 0;
		for(int times=0; times<10; times++){
			// shuffle pairs
			unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // shuffle them
			     ::shuffle(pairs_indexes.begin(), pairs_indexes.end(), default_random_engine(seed));
			int offset = 0;
			vector<int> subset(&pairs_indexes[offset],&pairs_indexes[offset+subset_size]); // get 1/10 of the pairs

			// remove actual ratings, so that we can predict them
			vector<vector <double>> edit_users = users;
			vector<vector<int>> edit_users_tweets = users_tweets;

			// edit the user's space, so that we can predict items
			int n = 0;
			for(int i=0; i<subset.size(); i++){
				//cout << "Subset-" << i << endl;
				pair<int,int> pr = pairs[subset[i]];
				edit_users[pr.first][pr.second] = 0;
				n++;
				edit_users_tweets[pr.first].erase(remove(edit_users_tweets[pr.first].begin(), edit_users_tweets[pr.first].end(), pr.second), edit_users_tweets[pr.first].end());
			}

		 									// Predict erased values
			// Unrated users will get the mean score
			for(int i=0; i<clusters.size(); i++){
				double R=0; // average score
				int rated_items=0; // number of rated items
				for(int j=0; j<clusters[i].size(); j++){
					if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) != clusters_tweets[i].end()){
						rated_items++;
						R += clusters[i][j];
					}
				}
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j<users[i].size(); j++){
					if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) == clusters_tweets[i].end()){
						clusters[i][j] =  R;
					}
				}
			}

			// create the Vector Space
			VectorSpace users_vector_space(&clustersIDs,&clusters);

			// create the Hash Tables And Hash Vector Space
			int k=4, L=5;
			string metric = "cosine";
			vector<int> r;
			for(int i=0; i<k ; i++)
				r.push_back((rand() % 10));
			vector <HashTable> LTables ;
			for(int i=0; i<L; i++){
				HashTable Li(k,r,users_vector_space.getDimension()); // create a hash table
				LTables.push_back(Li); // push it to the vector with the hash tables
				LTables[i].HashVectorSpace(&users_vector_space,k,metric); // fill each hash table after hashing a vector space
			}
			vector<int> Result,results;

			// for each user in validation subset predict the erased value
			double MAE = 0; // mean absolute error
			for(int i=0; i<subset.size(); i++){
				pair<int,int> pr = pairs[subset[i]];
				if((all_of(edit_users[pr.first].begin(), edit_users[pr.first].end(), [](double i) { return i==0.0; })))
					continue;
				// LSH-Cosine Range Search
				for(int l=0 ; l<L ; l++){
					// get user's nearest neighbors
					results = (LTables)[l].RangeSearch(0,&users_vector_space,&(edit_users[pr.first]),k,users_vector_space.getNumberOfPoints(),metric);
					int points_in_bucket = 0;
					for(uint j=0; j < results.size(); j++){
						Result.push_back(results[j]); // get all neighbors
					}
				}
				sort(Result.begin(),Result.end());
				Result.erase(unique(Result.begin(),Result.end()),Result.end());
				if(Result.size() == 0)
					cout << "Empty" << endl;

				// Keep the P closest neighbors-users
				vector<vector<double>> neighboring_users;
				vector<double> nearest_neighbors_dists;
				vector<int> nearest_neighbors_ids;
				for(uint p1 = 0 ; p1 < Result.size() ; p1++){
					if(neighboring_users.size() < p){
						// check if users has only zeros
						if(all_of(clusters[Result[p1]].begin(), clusters[Result[p1]].end(), [](double i) { return i==0.0; }))
							continue;
						neighboring_users.push_back(clusters[Result[p1]]);
						nearest_neighbors_dists.push_back(cosine_distance(&clusters[Result[p1]],&edit_users[pr.first]));
						nearest_neighbors_ids.push_back(Result[p1]);
					}
					else{
						// check if users has only zeros
						if(all_of(clusters[Result[p1]].begin(), clusters[Result[p1]].end(), [](double i) { return i==0.0; }))
							continue;
						int max_index;
						double max_distance = -1;
						// find the neighbor that is the furthest
						for(int p2=0; p2<p; p2++){
							if( nearest_neighbors_dists[p2] > max_distance){
								max_distance = nearest_neighbors_dists[p2];
								max_index = p2;
							}
						}
						// check if current neighbor is closest than furthest
						double curr_distance = cosine_distance(&edit_users[Result[p1]],&edit_users[pr.first]);
						if(curr_distance < max_distance){
							neighboring_users[max_index] = clusters[Result[p1]];
							nearest_neighbors_dists[max_index] = curr_distance;
							nearest_neighbors_ids[max_index] = Result[p1];
						}
					}
				}

				// Νormalization: user's vector
				vector<double> user = edit_users[pr.first];
				double R=0; // average score
				int rated_items=0; // number of rated items
				for(int j=0; j<user.size(); j++){
					if(find(edit_users_tweets[pr.first].begin(),edit_users_tweets[pr.first].end(),j) != edit_users_tweets[pr.first].end()){
						rated_items++;
						R += user[j];
					}
				}
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j<user.size(); j++){
					if(find(edit_users_tweets[pr.first].begin(),edit_users_tweets[pr.first].end(),j) != edit_users_tweets[pr.first].end()){
						user[j] = user[j] - R;
					}
					else{
						user[j] = 0;
					}
				}

				// Νormalization: neighbors' vector
				for(int i=0; i<neighboring_users.size(); i++){
					double R=0; // average score
					int rated_items=0; // number of rated items
					int nn_id = nearest_neighbors_ids[i]; // the id of neighbor
					for(int j=0; j<neighboring_users[i].size(); j++){
						if(find(clusters_tweets[nn_id].begin(),clusters_tweets[nn_id].end(),j) != clusters_tweets[nn_id].end()){
							rated_items++;
							R += neighboring_users[i][j];
						}
					} // calculate average
					if(rated_items !=0)
						R /= rated_items;
					for(int j=0; j<neighboring_users[i].size(); j++){
						if(find(clusters_tweets[nn_id].begin(),clusters_tweets[nn_id].end(),j) != clusters_tweets[nn_id].end()){
							neighboring_users[i][j] = neighboring_users[i][j] - R;
						}
						else{
							neighboring_users[i][j] = 0;
						}
					}
				}

				// Predict Scores with P Nearest Neighbors
				double z=0;
				for(uint p1=0 ; p1<neighboring_users.size() ; p1++){ // for each neighbor
					// For each cryptocurrency that user doesn't have opinion
						double sim = (1-nearest_neighbors_dists[p1]); // calculate similarity based pre-normalization
						//double sim = similarity(&user,&neighboring_users[p1]);
						user[pr.second] += sim*neighboring_users[p1][pr.second];
						z += abs(sim);
				}
				z = 1/z;
				double predict_score = z*user[pr.second];

				MAE += abs(users[pr.first][pr.second] - predict_score);
				//getchar();
			}
			MAE /= subset.size();
			//cout << MAE << endl;
			average_MAE += MAE;
		}
		average_MAE /= 10;
		cout << "Cosine LSH recommendation, based on virtual users, MAE: " << average_MAE << endl;
		out << "Cosine LSH recommendation, based on virtual users, MAE: " << average_MAE << endl;
	}
	out.close();
}

// start recommending cryptos to users with Clustering methods
void RecommendationSystem::start_recommendationClustering(string vectors,string output_file){
	ofstream out;
	out.open(output_file);
	auto start = chrono::steady_clock::now();
	// recommendation based on user opinions after sentiment analysis
	if(vectors == "users"){
		out << "Clustering on users: " << endl;
		string configuration_file= "./cluster.conf";
		bool complete = true;

		// Νormalization
		vector<vector<double>> users_normalized = users;
		for(int i=0; i<users_normalized.size(); i++){
			double R=0; // average score
			int rated_items=0; // number of rated items
			for(int j=0; j<users_normalized[i].size(); j++){
				if(find(users_tweets[i].begin(),users_tweets[i].end(),j) != users_tweets[i].end()){
					rated_items++;
					R += users_normalized[i][j];
				}
			} // calculate average
			if(rated_items !=0)
				R /= rated_items;
			for(int j=0; j< users_normalized[i].size(); j++){
				if(find(users_tweets[i].begin(),users_tweets[i].end(),j) != users_tweets[i].end()){
					users_normalized[i][j] = users_normalized[i][j] - R;
				}
				else{
					users_normalized[i][j] = 0;
				}
			}
		}

		// Cluster the users' vector space
		Clustering clustering(&usersIDs,&users_normalized,configuration_file,"kmeans++","lloyd","pam","euclidean");
		clustering.startClustering("./users_clustering",complete,"kmeans++","lloyd","pam","euclidean");

		// create clusters
		vector<vector<int>> clusters_vector;
		int noClusters = clustering.getNoClusters();
		for(int i=0; i<noClusters; i++){
			clusters_vector.push_back(vector<int> ());
		}
		for(int i=0; i<users.size(); i++){
			if((all_of(users[i].begin(), users[i].end(), [](double j) { return j==0.0; })))
				continue;
			int users_cluster = clustering.clusters[i]; // the cluster that user belongs
			clusters_vector[users_cluster].push_back(i);
		}

		// For each user calculate unknown cryptos cryptos
		for(int i=0; i<users.size(); i++){
			// user's that doesn't have an opinion will not be taken into accout
			if((all_of(users[i].begin(), users[i].end(), [](double i) { return i==0.0; })))
				continue;
			int users_cluster = clustering.clusters[i]; // the cluster that user belongs
			vector<double> user = users[i];
			// For each crypto
			for(int c=0; c<user.size(); c++){
				// that user doesn't have an opinion yet
				if(find(users_tweets[i].begin(),users_tweets[i].end(),c) == users_tweets[i].end()){
					double z=0;
					// Find all users
					for(int j=0; j<clusters_vector[users_cluster].size(); j++){
						int n = clusters_vector[users_cluster][j]; // which neighbor
						if( i != n ){ // don't compare with user's self
							double sim = similarity(&user,&users[n]);
							user[c] += sim*users_normalized[n][c];
							z += abs(sim);
						}
					}
					if(z != 0){
						z = 1/z;
						double predict_score = z*user[c];
						user[c] = predict_score;
					}
				}
			}

			// Find the 5 best cryptocurrencies
			vector<int> best_cryptos;
			vector<double> best_cryptos_scores;
			for(int c=0; c<user.size(); c++){
				// we need to reccomend bitcoins that user has never refered to
				if(find(users_tweets[i].begin(),users_tweets[i].end(),c) != users_tweets[i].end())
					continue;
				if(best_cryptos.size() < 5){
					best_cryptos.push_back(c);
					best_cryptos_scores.push_back(user[c]);
				}
				else{
					int min_index;
					double min_score = 100000;
					// find the worst score
					for(int j=0; j<5; j++){
						if( best_cryptos_scores[j] < min_score ){
							min_score = best_cryptos_scores[j];
							min_index = j;
						}
					}
					// check if current score is better than worse
					if( user[c] >  min_score){
						best_cryptos_scores[min_index] = user[c];
						best_cryptos[min_index] = c;
					}
				}
			}
			cout << "User: " << usersIDs[i] << endl;
			out << usersIDs[i] << ": ";
			cout << "Best Scores: " << endl;
			for(int c=0; c<best_cryptos_scores.size(); c++){
				cout << cryptocurrencies[best_cryptos[c]][0] << " ";
				out << cryptocurrencies[best_cryptos[c]][0] << " ";
				//cout << best_cryptos_scores[c] << endl;
			}
			out << endl;
			cout << endl;
			//getchar();
		}

	}
	// recommendation based on clusters opinions after TF-IDF analysis
	if(vectors == "clusters"){
		out << "Clustering on clusters" << endl;
		string configuration_file= "./cluster.conf";
		bool complete = true;

		// Νormalization
		vector<vector<double>> clusters_normalized = clusters;
		for(int i=0; i<clusters_normalized.size(); i++){
			double R=0; // average score
			int rated_items=0; // number of rated items
			for(int j=0; j<clusters_normalized[i].size(); j++){
				if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) != clusters_tweets[i].end()){
					rated_items++;
					R += clusters_normalized[i][j];
				}
			} // calculate average
			if(rated_items !=0)
				R /= rated_items;
			for(int j=0; j< clusters_normalized[i].size(); j++){
				if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) != clusters_tweets[i].end()){
					clusters_normalized[i][j] = clusters_normalized[i][j] - R;
				}
				else{
					clusters_normalized[i][j] = 0;
				}
			}
		}

		// For each user calculate unknown cryptos cryptos
		for(int i=0; i<users.size(); i++){
			// user's that doesn't have an opinion will not be taken into accout
			if((all_of(users[i].begin(), users[i].end(), [](double i) { return i==0.0; })))
				continue;

			// Unrated users will get the mean score of the rated cryptos
			for(int i=0; i<users.size(); i++){
				double R=0; // average score
				int rated_items=0; // number of rated items
				for(int j=0; j<users[i].size(); j++){
					// find those tweet that user has a rating
					if(find(users_tweets[i].begin(),users_tweets[i].end(),j) != users_tweets[i].end()){
						rated_items++;
						R += users[i][j];
					}
				}
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j<users[i].size(); j++){
					if(find(users_tweets[i].begin(),users_tweets[i].end(),j) == users_tweets[i].end()){
						users[i][j] =  R;
					}
				}
			}

			clusters_normalized.push_back(users[i]); // push user in to find the cluster belonging
			clustersIDs.push_back(usersIDs[i]);

			// Cluster the clusters' vector space
			Clustering clustering(&clustersIDs,&clusters_normalized,configuration_file,"kmeans++","lloyd","pam","euclidean");
			clustering.startClustering("./clusters_clustering",complete,"kmeans++","lloyd","pam","euclidean");

			// create clusters
			vector<vector<int>> clusters_vector;
			int noClusters = clustering.getNoClusters();
			for(int i=0; i<noClusters; i++){
				clusters_vector.push_back(vector<int> ());
			}
			for(int i=0; i<clusters.size(); i++){
				if((all_of(clusters[i].begin(), clusters[i].end(), [](double j) { return j==0.0; })))
					continue;
				int v_users_cluster = clustering.clusters[i]; // the cluster that user belongs
				clusters_vector[v_users_cluster].push_back(i);
			}


			int users_cluster = clustering.clusters[i]; // the cluster that user belongs
			vector<double> user = users[i];
			// For each crypto
			for(int c=0; c<user.size(); c++){
				// that user doesn't have an opinion yet
				if(find(users_tweets[i].begin(),users_tweets[i].end(),c) == users_tweets[i].end()){
					double z=0;
					// Find all users
					for(int j=0; j<clusters_vector[users_cluster].size(); j++){
						int n = clusters_vector[users_cluster][j]; // which neighbor
						if( (i != n) ){ // don't compare with user's self
							double sim = similarity(&user,&clusters[n]);
							user[c] += sim*clusters_normalized[n][c];
							z += abs(sim);
						}
					}
					if(z != 0){
						z = 1/z;
						double predict_score = z*user[c];
						user[c] = predict_score;
					}
				}
			}

			// Find the 2 best cryptocurrencies
			vector<int> best_cryptos;
			vector<double> best_cryptos_scores;
			for(int c=0; c<user.size(); c++){
				// we need to reccomend bitcoins that user has never refered to
				if(find(users_tweets[i].begin(),users_tweets[i].end(),c) != users_tweets[i].end())
					continue;
				if(best_cryptos.size() < 2){
					best_cryptos.push_back(c);
					best_cryptos_scores.push_back(user[c]);
				}
				else{
					int min_index;
					double min_score = 100000;
					// find the worst score
					for(int j=0; j<2; j++){
						if( best_cryptos_scores[j] < min_score ){
							min_score = best_cryptos_scores[j];
							min_index = j;
						}
					}
					// check if current score is better than worse
					if( user[c] >  min_score){
						best_cryptos_scores[min_index] = user[c];
						best_cryptos[min_index] = c;
					}
				}
			}
			cout << "User: " << usersIDs[i] << endl;
			out << usersIDs[i]  << ": " << endl;
			cout << "Best Scores: " << endl;
			for(int c=0; c<best_cryptos_scores.size(); c++){
				cout << cryptocurrencies[best_cryptos[c]][0] << " ";
				out << cryptocurrencies[best_cryptos[c]][0] << " ";
				//cout << best_cryptos_scores[c] << endl;
			}
			out << endl;
			cout << endl;
			//getchar();
		}
	}
	auto end = chrono::steady_clock::now();
	cout << "Execution time : " << chrono::duration_cast<chrono::seconds>(end - start).count() << " sec" << endl;
	out << "Execution time : " << chrono::duration_cast<chrono::seconds>(end - start).count() << " sec" << endl;
	out.close();
}

// validate the recommendation of clustering method
void RecommendationSystem::validate_recommendationClustering(string vector_space,string output_file){
	ofstream out;
	out.open(output_file);
	// 10-fold cross validation on users
	if(vector_space=="users"){
		// store all rated items
		vector<pair <int,int>> pairs;
		for(int Ui=0; Ui<users.size(); Ui++){ // for each user
			for(int i=0; i<users_tweets[Ui].size(); i++){ // for each crypto referring
				pair<int,int> a_pair(Ui, users_tweets[Ui][i]);
				if (find(pairs.begin(), pairs.end(), a_pair) == pairs.end()) {
					pairs.push_back(a_pair); // (user,crypto)
				}
			}
		}
		// split pairs to 10 subsets for 10-fold cross validation
		int number_of_pairs = pairs.size();
		int subset_size = number_of_pairs/10;
		vector<int> pairs_indexes;
		for(int i=0; i<number_of_pairs; i++) // store all indexes
			pairs_indexes.push_back(i);
		unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // shuffle them
		     ::shuffle(pairs_indexes.begin(), pairs_indexes.end(), default_random_engine(seed));
		vector<vector<int>> subsets(10,vector<int>());
		int offset = 0;
		for(int i=0; i<10; i++){ // load each subset
			vector<int> subset(&pairs_indexes[offset],&pairs_indexes[offset+subset_size]);
			subsets[i] = subset;
			offset += offset + 1;
		}

		// 10-fold cross validation
		double average_MAE = 0;
		for(int validation_set=0; validation_set<10; validation_set++){
			// remove actual ratings, so that we can predict them
			vector<vector <double>> edit_users = users;
			vector<vector<int>> edit_users_tweets = users_tweets;

			// edit the user's space, so that we can predict items
			int n = 0;
			for(int i=0; i<subsets[validation_set].size(); i++){
				//cout << "Subset-" << i << endl;
				pair<int,int> pr = pairs[subsets[validation_set][i]];
				edit_users[pr.first][pr.second] = 0;
				n++;
				edit_users_tweets[pr.first].erase(remove(edit_users_tweets[pr.first].begin(), edit_users_tweets[pr.first].end(), pr.second), edit_users_tweets[pr.first].end());
			}

		 						   // Predict erased values
			string configuration_file= "./cluster.conf";
			bool complete = true;

			// Νormalization
			vector<vector<double>> users_normalized = edit_users;
			for(int i=0; i<users_normalized.size(); i++){
				double R=0; // average score
				int rated_items=0; // number of rated items
				for(int j=0; j<users_normalized[i].size(); j++){
					if(find(edit_users_tweets[i].begin(),edit_users_tweets[i].end(),j) != edit_users_tweets[i].end()){
						rated_items++;
						R += users_normalized[i][j];
					}
				} // calculate average
				if(rated_items !=0)
					R /= rated_items;
				for(int j=0; j< users_normalized[i].size(); j++){
					if(find(edit_users_tweets[i].begin(),edit_users_tweets[i].end(),j) != edit_users_tweets[i].end()){
						users_normalized[i][j] = users_normalized[i][j] - R;
					}
					else{
						users_normalized[i][j] = 0;
					}
				}
			}

			// Cluster the users' vector space
			Clustering clustering(&usersIDs,&users_normalized,configuration_file,"kmeans++","lloyd","pam","euclidean");
			clustering.startClustering("./users_clustering_validation",complete,"kmeans++","lloyd","pam","euclidean");

			// create clusters
			vector<vector<int>> clusters_vector;
			int noClusters = clustering.getNoClusters();
			for(int i=0; i<noClusters; i++){
				clusters_vector.push_back(vector<int> ());
			}
			for(int i=0; i<users.size(); i++){
				if((all_of(edit_users[i].begin(), edit_users[i].end(), [](double j) { return j==0.0; })))
					continue;
				int users_cluster = clustering.clusters[i]; // the cluster that user belongs
				clusters_vector[users_cluster].push_back(i);
			}

			// for each user in validation subset predict the erased value
			double MAE = 0; // mean absolute error
			for(int i=0; i<subsets[validation_set].size(); i++){
				pair<int,int> pr = pairs[subsets[validation_set][i]];
				if((all_of(edit_users[pr.first].begin(), edit_users[pr.first].end(), [](double i) { return i==0.0; })))
					continue;
				int users_cluster = clustering.clusters[pr.first]; // the cluster that user belongs
				vector<double> user = users[pr.first];
				// predict the erase crypto based on clusters
				double z=0;
				// Find all users in cluster
				for(int j=0; j<clusters_vector[users_cluster].size(); j++){
					int n = clusters_vector[users_cluster][j]; // which neighbor
					if( pr.first != n ){ // don't compare with user's self
						double sim = similarity(&user,&edit_users[n]);
						user[pr.second] += sim*users_normalized[n][pr.second];
						z += abs(sim);
					}
				}
				if(z != 0){
					z = 1/z;
					double predict_score = z*user[pr.second];
					user[pr.second] = predict_score;
					MAE += abs(users[pr.first][pr.second] - predict_score);
				}
				//getchar();
			}
			MAE /= subsets[validation_set].size();
			//cout << MAE << endl;
			average_MAE += MAE;
		}
		average_MAE /= 10;
		cout << "Clustering recommendation, based on real users, MAE: " << average_MAE << endl;
		out << "Clustering recommendation, based on real users, MAE: " << average_MAE << endl;
	}
	// random validation on clusters
	if(vector_space=="clusters"){
		// store all rated items
		vector<pair <int,int>> pairs;
		for(int Ui=0; Ui<users.size(); Ui++){ // for each user
			for(int i=0; i<users_tweets[Ui].size(); i++){ // for each crypto referring
				pair<int,int> a_pair(Ui, users_tweets[Ui][i]);
				if (find(pairs.begin(), pairs.end(), a_pair) == pairs.end()) {
					pairs.push_back(a_pair); // (user,crypto)
				}
			}
		}
		// split pairs to 10 subsets for 10-fold cross validation
		int number_of_pairs = pairs.size();
		int subset_size = number_of_pairs/1000;
		vector<int> pairs_indexes;
		for(int i=0; i<number_of_pairs; i++) // store all indexes
			pairs_indexes.push_back(i);

		// validation
		double average_MAE = 0;
		for(int times=0; times<1; times++){
			// shuffle pairs
			unsigned seed = chrono::system_clock::now().time_since_epoch().count(); // shuffle them
					 ::shuffle(pairs_indexes.begin(), pairs_indexes.end(), default_random_engine(seed));
			int offset = 0;
			vector<int> subset(&pairs_indexes[offset],&pairs_indexes[offset+subset_size]); // get 1/10 of the pairs

			// remove actual ratings, so that we can predict them
			vector<vector <double>> edit_users = users;
			vector<vector<int>> edit_users_tweets = users_tweets;

			// edit the user's space, so that we can predict items
			int n = 0;
			for(int i=0; i<subset.size(); i++){
				//cout << "Subset-" << i << endl;
				pair<int,int> pr = pairs[subset[i]];
				edit_users[pr.first][pr.second] = 0;
				n++;
				edit_users_tweets[pr.first].erase(remove(edit_users_tweets[pr.first].begin(), edit_users_tweets[pr.first].end(), pr.second), edit_users_tweets[pr.first].end());
			}
				   			// Predict erased values
			string configuration_file= "./cluster.conf";
			bool complete = true;

			// Νormalization
			vector<vector<double>> clusters_normalized = clusters;
			for(int i=0; i<clusters_normalized.size(); i++){
				double R=0; // average score
				int rated_items=0; // number of rated items
				for(int j=0; j<clusters_normalized[i].size(); j++){
				 	if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) != clusters_tweets[i].end()){
					 	rated_items++;
					 	R += clusters_normalized[i][j];
					}
			 	} // calculate average
				if(rated_items !=0)
				 R /= rated_items;
				for(int j=0; j< clusters_normalized[i].size(); j++){
					if(find(clusters_tweets[i].begin(),clusters_tweets[i].end(),j) != clusters_tweets[i].end()){
						clusters_normalized[i][j] = clusters_normalized[i][j] - R;
				 	}
				 	else{
					 	clusters_normalized[i][j] = 0;
				 	}
				}
			}

			// For each user calculate unknown cryptos cryptos
			double MAE=0;
			for(int i=0; i<subset.size(); i++){
				pair<int,int> pr = pairs[subset[i]];
				// user's that doesn't have an opinion will not be taken into accout
				if((all_of(edit_users[pr.first].begin(), edit_users[pr.first].end(), [](double i) { return i==0.0; })))
					continue;

				// Unrated users will get the mean score of the rated cryptos
				for(int e=0; e<edit_users.size(); e++){
					double R=0; // average score
					int rated_items=0; // number of rated items
					for(int j=0; j<edit_users[e].size(); j++){
						// find those tweet that user has a rating
						if(find(edit_users_tweets[e].begin(),edit_users_tweets[e].end(),j) != edit_users_tweets[e].end()){
							rated_items++;
							R += users[e][j];
						}
					}
					if(rated_items !=0)
					 R /= rated_items;
					for(int j=0; j<edit_users[e].size(); j++){
						if(find(users_tweets[e].begin(),users_tweets[e].end(),j) == users_tweets[e].end()){
					  	users[e][j] =  R;
						}
					}
				}
				clusters_normalized.push_back(edit_users[pr.first]); // push user in to find the cluster belonging
				clustersIDs.push_back(usersIDs[pr.first]);

				// Cluster the clusters' vector space
				Clustering clustering(&clustersIDs,&clusters_normalized,configuration_file,"kmeans++","lloyd","pam","euclidea");
				clustering.startClustering("./clusters_clustering",complete,"kmeans++","lloyd","pam","euclidean");

				// create clusters
				vector<vector<int>> clusters_vector;
				int noClusters = clustering.getNoClusters();
				for(int cl=0; cl<noClusters; cl++){
				 clusters_vector.push_back(vector<int> ());
				}
				for(int cl=0; cl<clusters.size(); cl++){
					if((all_of(clusters[cl].begin(), clusters[cl].end(), [](double j) { return j==0.0; })))
						continue;
					int v_users_cluster = clustering.clusters[cl]; // the cluster that user belongs
					clusters_vector[v_users_cluster].push_back(cl);
				}


				int users_cluster = clustering.clusters[pr.first]; // the cluster that user belongs
				if((users_cluster>0) && (users_cluster<noClusters)){
					continue;
				}
				vector<double> user = users[pr.first];
				// predict unknown crypto
				double z=0;
				// Find all users
			 	for(int j=0; j<clusters_vector[users_cluster].size(); j++){
					int n = clusters_vector[users_cluster][j]; // which neighbor
					if( (pr.first != n) ){ // don't compare with user's self
						double sim = similarity(&user,&clusters[n]);
						user[pr.second] += sim*clusters_normalized[n][pr.second];
						z += abs(sim);
					}
				}
				if(z != 0){
					z = 1/z;
					double predict_score = z*user[pr.second];
					user[pr.second] = predict_score;
					MAE += abs(users[pr.first][pr.second] - predict_score);
				}
			}
			MAE /= subset.size();
			//cout << MAE << endl;
			average_MAE += MAE;
		}
		average_MAE /= 10;
		cout << "Clustering recommendation, based on virtual users, MAE: " << average_MAE << endl;
		out << "Clustering recommendation, based on virtual users, MAE: " << average_MAE << endl;
	}
	out.close();
}

// print the contents of each lexicon
void RecommendationSystem::print_lexicons(){
  // a_lexicon
  cout << "Printing vader lexicon:" << endl;
  for (auto x : vader_lexicon)
       cout << x.first << " " << x.second << endl;
  getchar();
  // k_lexicon
  cout << "Printing cryptocurrency lexicon:" << endl;
  for(int i=0; i<cryptocurrencies.size(); i++){
    for(int j=0; j<cryptocurrencies[i].size(); j++)
      cout << cryptocurrencies[i][j] << " ";
    cout << endl;
  }
  cout << "There are " << numCryptos << " cryptocurrencies" << endl;
  getchar();
  // sentiment_analysis
  for (auto x : proccessed_tweets){
    cout << "Tweet: " << x.first << " has sentiment score " << x.second.first << " and it refers to:";
    cout << "[";
    for(int i=0; i<x.second.second.size(); i++)
      cout << x.second.second[i] << ",";
    cout << "]" << endl;
  }
  cout << "There are " << proccessed_tweets.size() << " tweets"<< endl;
  getchar();
}

// print users
void RecommendationSystem::print_users(){
  for(int i=0; i<usersIDs.size(); i++){
    cout << "UserID: " << usersIDs[i] << " , ";
    for(int j=0; j<users[i].size(); j++)
      cout << users[i][j] << " ";
		cout << endl;
		for(int j=0; j<users_tweets[i].size(); j++)
			cout << users_tweets[i][j] << " , ";
		cout << endl;
  }
	getchar();
}

// print clusters
void RecommendationSystem::print_clusters(){
  for(int i=0; i<clustersIDs.size(); i++){
    cout << "clusterID: " << clustersIDs[i] << " , ";
    for(int j=0; j<clusters[i].size(); j++)
      cout << clusters[i][j] << " ";
    cout << endl;
  }
	getchar();
}
