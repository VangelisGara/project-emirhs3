#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "../headers/Insights.h"
#include "../headers/MathHelper.h"
#include "../headers/VectorSpace.h"
using namespace std;

// vector space constructor
VectorSpace::VectorSpace(vector <int>* ids,vector<vector <double>> *vector_space){
  Dspace = *vector_space;
  itemIds = *ids;
  dimension = Dspace[0].size();
  number_of_points = itemIds.size();
}

// vector space destructor
VectorSpace::~VectorSpace(){
    //cout << "Vector space will be destroyed soon" << '\n';
}

// Getters
int VectorSpace::getDimension(){
  return dimension;
}

int VectorSpace::getNumberOfPoints(){
  return number_of_points;
}

vector<int>* VectorSpace::getIds(){
  return &itemIds;
}

// returns the id of the nearest neighbor
int VectorSpace::NearestNeighbor(vector <double>* q,string metric){
  double temp , min = 1000000.0;
  int nn, nup = number_of_points, dim = dimension;
  if(metric == "euclidean"){
    for(int i=0; i< nup ; i++){
      temp = euclidean_distance(&(Dspace[i]),q);
      if(temp < min){
        min = temp;
        nn = itemIds[i];
      }
    }
  }
  else if(metric == "cosine"){
    for(int i=0; i< nup ; i++){
      temp = cosine_distance(&(Dspace[i]),q);
      if(temp < min){
        min = temp;
        nn = itemIds[i];
      }
    }
  }
  //cout << "distanceTrue: " << min << endl;
  return nn;
}

// Print vector space elements
void VectorSpace::printVectorSpace(){
  int nup = number_of_points;
  int dim = dimension;
  for(int i=0; i< nup ; i++){
    cout << "--------------------------" << endl;
    cout << itemIds[i] << endl;
    for(int j=0; j<dimension ; j++)
      cout << Dspace[i][j] << " ";
    cout << endl;
  }
  //cout << "Vector space with " << nup << " , "<< dim << " dimensions" << endl;
}
