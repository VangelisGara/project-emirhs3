#ifndef RECOMMENDATIONSYSTEM_H
#define RECOMMENDATIONSYSTEM_H

#include <unordered_map>
#include <vector>

using namespace std;

class RecommendationSystem{
  unordered_map<string, double> vader_lexicon; // lexicon containing words and score
  vector<vector<string>> cryptocurrencies; // lexicon containing all cryptocurrencies and different names
  unordered_map<int,pair<double,vector<int>>> proccessed_tweets; // tweet id , sentiment score and crypto refering to
  int numCryptos=0; // number of cryptos in crypto lexicon
  // For Recommendation Based On Users Tweets and Sentiment Analysis
  vector<int> usersIDs; // the id of each user
  vector<vector<double>> users; // vectors for user opinions
  vector<vector<int>> users_tweets; // for each user store the tweets that he has been refered to
  // For Recommendation Based On Clusters produces by IDF text processing
  vector<int> clustersIDs; // the id of each cluster
  vector<vector<double>> clusters; // vectors of clusters opinions
  vector<vector<int>> clusters_tweets; // for each cluster store the tweets that has been refered to
  public:
  RecommendationSystem(string tweets_path,string lexicon_path,string cryptos_path,string clusters_file,string vector_space); // Constructor
  void lexicon_analyzer(string lexicon_path); // gets a csv file and produces a dictionary like umap
  void cryptocurrencies_analyzer(string cryptos_path); // gets a csv file and produces a list like 2d vector
  void tweets_analysis(string tweets_path); // processes the tweets
  void clusters_analysis(string clusters_path); // process the clusters
  void start_recommendationLSH(string vectors,string output_file,int p); // start recommending cryptos to users with LSH range search
  void start_recommendationClustering(string vectors,string output_file); // start recommending cryptos to users with Clustering methods
  void validate_recommendationLSH(string vector_space,string output_file,int p);  // validate the recommendation
  void validate_recommendationClustering(string vector_space,string output_file);  // validate the recommendation of clustering method
  void print_lexicons(); // print lexicon contents
  void print_users(); // print users
  void print_clusters(); // print clusters
};

#endif
